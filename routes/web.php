<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Auth::routes();

Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::group(['prefix' => 'customers', 'as' => 'customers.'], function () {
        Route::get('/', 'App\Http\Controllers\CustomerController@index')->name('index');
        Route::get('edit/{customer}/customer', 'App\Http\Controllers\CustomerController@edit')->name('edit');
        Route::get('create', 'App\Http\Controllers\CustomerController@create')->name('create');
        Route::post('store', 'App\Http\Controllers\CustomerController@store')->name('store');
        Route::post('u/{customer}/update', 'App\Http\Controllers\CustomerController@update')->name('update');
        Route::get('d/{customer}/delete', 'App\Http\Controllers\CustomerController@destroy')->name('delete');
    });
	
	Route::group(['prefix' => 'transactions', 'as' => 'transactions.'], function () {
        Route::get('/', 'App\Http\Controllers\TransactionController@index')->name('index');
        Route::get('edit/{transaction}/transaction', 'App\Http\Controllers\TransactionController@edit')->name('edit');
        Route::get('select', 'App\Http\Controllers\TransactionController@select')->name('select');
        Route::get('create', 'App\Http\Controllers\TransactionController@create')->name('create');
        Route::post('store', 'App\Http\Controllers\TransactionController@store')->name('store');
        Route::post('u/{transaction}/update', 'App\Http\Controllers\TransactionController@update')->name('update');
        Route::get('d/{transaction}/delete', 'App\Http\Controllers\TransactionController@destroy')->name('delete');
    });

	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
});

Route::group(['middleware' => 'auth'], function () {
	Route::get('{page}', ['as' => 'page.index', 'uses' => 'App\Http\Controllers\PageController@index']);
});

