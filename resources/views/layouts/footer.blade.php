<footer class="footer footer-black  footer-white ">
    <div class="container-fluid">
        <div class="row">
            <nav class="footer-nav">
                <ul>
                    <li>
                        <a href="https://github.com/jacksiroke" target="_blank">{{ __('Github') }}</a>
                    </li>
                    <li>
                        <a href="https://twitter.com/jacksiroke" target="_blank">{{ __('Twitter') }}</a>
                    </li>
                    <li>
                        <a href="http://linkedin.com/in/jacksiroke/" target="_blank">{{ __('LinkedIn') }}</a>
                    </li>
                    <li>
                        <a href="https://medium.com/@jacksiroke" target="_blank">{{ __('Medium') }}</a>
                    </li>
                </ul>
            </nav>
            <div class="credits ml-auto">
                <span class="copyright">
                    ©
                    <script>
                        document.write(new Date().getFullYear())
                    </script>{{ __(', made with ') }}<i class="fa fa-heart heart"></i>{{ __(' by ') }}<a class="@if(Auth::guest()) text-white @endif" href="https://appsmata.com" target="_blank">{{ __('@JacksiroKe') }}</a>
                </span>
            </div>
        </div>
    </div>
</footer>