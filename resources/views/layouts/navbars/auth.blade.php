<div class="sidebar" data-color="white" data-active-color="danger">
    <div class="logo">
        <a href="." class="simple-text logo-mini">
            <div class="logo-image-small">
                <img src="{{ asset('paper') }}/img/logo-small.png">
            </div>
        </a>
        <a href="." class="simple-text logo-normal">
            {{ __('JacksiroKe') }}
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="{{ $elementActive == 'customers' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'customers') }}">
                    <i class="nc-icon nc-diamond"></i>
                    <p>{{ __('Customers') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'transactions' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'transactions') }}">
                    <i class="nc-icon nc-bank"></i>
                    <p>{{ __('Transactions') }}</p>
                </a>
            </li>
        </ul>
    </div>
</div>
