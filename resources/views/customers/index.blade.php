@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'customers'
])

@section('content')
    <div class="content">        
        @if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('status') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col">
                    <div class="card shadow">
                        <div class="card-header border-0">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">Customers</h3>
                                </div>
                                <div class="col-4 text-right">
                                    <a href="{{ route('customers.create') }}" class="btn btn-sm btn-primary">Add a Customer</a>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <table class="table align-items-center table-flush">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col">Name</th>
                                        <th scope="col">City</th>
                                        <th scope="col">Mobile</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Joined Date</th>
                                        <!--<th scope="col">Balance</th>-->
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($customers as $customer)
                                    <tr>
                                        <td>{{ $customer->name }}</td>
                                        <td>{{ $customer->city }}</td>
                                        <td>{{ $customer->mobile }}</td>
                                        <td>{{ $customer->email }}</td>
                                        <td>{{ $customer->created_at }}</td>
                                        <!--<td align="right">{{ $customer->balance }}</td>-->
                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                    <a class="dropdown-item" href="{{ route('customers.edit', $customer->id) }}">Edit Customer</a>
                                                    <a class="dropdown-item" href="{{ route('customers.delete', $customer->id) }}" onclick="return confirm('Are you sure you want to delete: {{ $customer->name }} from the system? \nBe careful, this action can not be reversed.')">Delete Customer</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
        
                        
                        <div class="card-footer py-4">
                            <nav class="d-flex justify-content-end" aria-label="...">
                                
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection