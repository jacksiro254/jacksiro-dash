@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'transactions'
])

@section('content')
    <div class="content">        
        @if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('status') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col">
                    <div class="card shadow">
                        <div class="card-header border-0">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">Transactions</h3>
                                </div>
                                <div class="col-4 text-right">
                                    <a href="{{ route('transactions.select') }}" class="btn btn-sm btn-primary">Add a Transaction</a>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <table class="table align-items-center table-flush">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col">Date/Time</th>
                                        <th scope="col">Sender Name</th>
                                        <th scope="col">Beneficiary</th>
                                        <th scope="col">Amount</th>
                                        <th scope="col">Ref. Code</th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($transactions as $transaction)
                                    <tr>
                                        <td>{{ $transaction->created_at }}</td>
                                        <td>{{ $transaction->senderid }}</td>
                                        <td>{{ $transaction->receiverid }}</td>
                                        <td>{{ $transaction->amount }}</td>
                                        <td>{{ $transaction->code }}</td>
                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                    <a class="dropdown-item" href="{{ route('transactions.delete', $transaction->id) }}" onclick="return confirm('Are you sure you want to delete this transaction from the system? \nBe careful, this action can not be reversed.')">Delete Transaction</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
        
                        
                        <div class="card-footer py-4">
                            <nav class="d-flex justify-content-end" aria-label="...">
                                
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection