@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'transactions'
])

@section('content')
    <div class="content">
        @if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('status') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="row">                
            <div class="col-md-8 text-center">
                <form class="col-md-12" action="{{ route('transactions.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="sender" value="{{ request('sender') }}" required>
                    <input type="hidden" name="code" value="OGA<?php $time_stamp = "'" . time() . "'";
                    echo $time_stamp[1].$time_stamp[4].$time_stamp[7].$time_stamp[10]; ?>X" required>
                    <div class="card">
                        <div class="card-header">
                            <h5 class="title">{{ __('New Transaction') }}</h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <label class="col-md-3 col-form-label">{{ __('Select Receiver') }}</label>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        @foreach ($customers as $customer)
                                            @if ($customer->id != request('sender'))
                                                <div class="row">
                                                    <label>
                                                        <input type="radio" name="receiver" value="{{ $customer->id }}" required>
                                                        {{ $customer->name }}
                                                    </label>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                    @if ($errors->has('receiver'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('receiver') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-md-3 col-form-label">{{ __('Amount (Kshs)') }}</label>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input type="number" name="amount" class="form-control" min="100" required>
                                    </div>
                                    @if ($errors->has('amount'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('amount') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="card-footer ">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-info btn-round">{{ __('Finish Transaction') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                
            </div>
        </div>
    </div>
@endsection