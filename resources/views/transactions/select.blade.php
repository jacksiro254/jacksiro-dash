@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'transactions'
])

@section('content')
    <div class="content">
        @if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('status') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="row">                
            <div class="col-md-8 text-center">
                <form class="col-md-12" action="{{ route('transactions.create') }}" method="GET">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="title">{{ __('New Transaction') }}</h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <label class="col-md-3 col-form-label">{{ __('Select Sender') }}</label>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        @foreach ($customers as $customer)
                                            <div class="row">
                                                <label>
                                                    <input type="radio" name="sender" value="{{ $customer->id }}" required>
                                                    {{ $customer->name }}
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                    @if ($errors->has('sender'))
                                        <span class="invalid-feedback" style="display: block;" role="alert">
                                            <strong>{{ $errors->first('sender') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="card-footer ">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-info btn-round">{{ __('Proceed with the Transaction') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                
            </div>
        </div>
    </div>
@endsection