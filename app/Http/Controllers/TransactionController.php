<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    /**
     * Display a listing of the transactions
     *
     * @param  \App\Models\Transaction  $model
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $transactions = Transaction::select([
            '*', 
            DB::raw('(SELECT name FROM customers WHERE customers.id = transactions.sender) as senderid'),
            DB::raw('(SELECT name FROM customers WHERE customers.id = transactions.receiver) as receiverid')
            ])->get();
        return view('transactions.index', compact('transactions'));
    }

    /**
     * Show the form for selecting a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function select()
    {
        $customers = Customer::latest()->paginate(100);
        return view('transactions.select', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = Customer::latest()->paginate(100);
        return view('transactions.create', compact('customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $rq
     * @return \Illuminate\Http\Response
     */
    public function store(Request $rq)
    {
        Transaction::create($rq->all());
        return redirect()->route('transactions.index')->with('success', 'Transaction created successfully.');
    }

    /**
     * Show the form for editing the profile.
     *
     * @return \Illuminate\View\View
     */
    public function edit(Transaction $transaction)
    {
        return view('transactions.edit', compact('transaction'));
    }

    /**
     * Show the form for viewing the profile.
     *
     * @return \Illuminate\View\View
     */
    public function view(Transaction $transaction)
    {
        return view('transactions.view', compact('transaction'));
    }

    /**
     * Update the resource
     *
     * @param  \App\Http\Requests\Request  $rq
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $rq, Transaction $transaction)
    {
        try {
            $transaction->update($rq->all());
            return redirect()->route('transactions.index')->with('success', 'Transaction updated successfully.');
        } catch (\Exception $exception) {
            return redirect()->back()->withError('Error while updating Transaction');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Apptime  $apptime
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        $transaction->delete();
        return redirect()->route('transactions.index')->with('success', 'Transaction deleted successfully');
    }
    
}
