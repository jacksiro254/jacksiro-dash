<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Transaction;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends Controller
{ 
    
    /**
    * Display a listing of the customers
    *
    * @param  \App\Models\Customer  $model
    * @return \Illuminate\View\View
    */
    public function getCustomers()
    {
        $token = Str::random(300);
        $results = Customer::select('id', 'name', 'mobile', 'city', 'email')->get();

        if (!$results)
            return response()->json(['response' => Response::HTTP_BAD_REQUEST], Response::HTTP_BAD_REQUEST);
        else
            return response()->json(['response' => Response::HTTP_OK, 'results' => $results], Response::HTTP_OK);
    }
    
    /**
    * Display a listing of the customers
    *
    * @param  \App\Models\Transaction  $model
    * @return \Illuminate\View\View
    */
    public function getTransactions()
    {
        $token = Str::random(300);
        $results = Transaction::select(
            'code', 
            'sender', 
            DB::raw('(SELECT name FROM customers WHERE customers.id = transactions.sender) as senderid'), 
            'receiver',
            DB::raw('(SELECT name FROM customers WHERE customers.id = transactions.receiver) as receiverid'),
            'amount',
            DB::raw('(SELECT UNIX_TIMESTAMP(created_at) FROM transactions) as datetime'),
        )->get();

        if (!$results)
            return response()->json(['response' => Response::HTTP_BAD_REQUEST], Response::HTTP_BAD_REQUEST);
        else
            return response()->json(['response' => Response::HTTP_OK, 'results' => $results], Response::HTTP_OK);
    }
}
